function judgeType(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
}

const s1 = '1';
const s2 = /[a-zA-Z]/;
console.log(judgeType(s1));
console.log(judgeType(s2));