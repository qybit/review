function Trim(s) {
    let new_str = '';
    let i = 0;
    while (s[i] === ' ') i ++;
    new_str = s.substr(i);
    let j = new_str.length - 1;
    while (new_str[j] === ' ') j --;
    new_str = new_str.substring(0, j + 1);
    return new_str;
}

console.log(`---${Trim('       abc    g          ')}---`)