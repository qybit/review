const myFlat = (arr) => {
    let res = [];
    for (let i = 0; i < arr.length; i ++) {
        if (Array.isArray(arr[i])) {
            res = res.concat(myFlat(arr[i]));
        } else {
            res.push(arr[i]);
        }
    }
    return res;
}

const t = [[12,3], 23, [2, [2, 2]]];
console.log(myFlat(t));