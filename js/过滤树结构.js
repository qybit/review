const tree = [
    { name: "A" },
    { name: "B", children: [{ name: "A" }] },
    {
      name: "AA",
      children: [
        { name: "BB" },
        { name: "C" },
        { name: "D", children: [{ name: "A" }] },
        { name: "A" },
      ],
    },
    { name: "D", children: [{ name: "C", children: [{ name: "AA" }] }] },
  ];
  function clear(tree, str) {
    if (!tree) return [];
    let res = [];
    for (let item of tree) {
        const it = {...item};
        if (it.name === str) {
            res.push(it);
        } else {
            let obj = clear(it.children, str);
            if (obj.length > 0) {
                it.children = obj;
                res.push(it);
            }
        }
    }
    return res;
  }
  console.log(JSON.stringify(clear(tree, "A")));