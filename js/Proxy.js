const o = {};

const proxy = new Proxy(o, {
    get(target, p, receiver) {
        console.log('get')
        return Reflect.get(...arguments);
    },
    set(target, p, value, receiver) {
        console.log('set');
        return Reflect.set(...arguments);
    }
});
proxy['a'] = 1;
proxy.b = 22;
console.log(proxy.a);
console.log(Object.is({a: 12}, {a: 12}));
