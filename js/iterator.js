// let arr = ['foo', 'bar'];
//
// class Counter {
//     constructor(limit) {
//         this.count = 1;
//         this.limit = limit;
//     }
//
//     next() {
//         if (this.count <= this.limit) {
//             return {done: false, value: this.count ++};
//         } else {
//             return {done: true, value: undefined};
//         }
//     }
//
//     [Symbol.iterator]() {
//         return this;
//     }
// }
//
// let cnt = new Counter(9);
//
// for (let c of cnt) {
//     console.log(c);
// }

// add(1)(2)(3) // 6

function curry(fn) {
    return function curried(...args) {
        if (fn.length >= args.length) {
            return fn.apply(this, args);
        } else {
            return curried(args.concat(fn.arguments));
        }
    }
}

const f = curry(function (...args){
    return args.reduce((pre, cur) => pre + cur, 0);
});
console.log(f(1, 2));
