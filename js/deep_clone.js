function Person(name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.introduce = function() {
    console.log(`my name is ${this.name}, and I'm ${this.age} years old!`);
}

const p = new Person('jj', 19);

const isObject = (obj) => obj && (typeof obj === 'object' || typeof obj === 'function');

function deepClone(origin, map = new WeakMap()) {
    if (map.has(origin)) return map.get(origin);
    const cor = origin.constructor;
    if (cor.name.toLowerCase() === 'regexp') return new cor();
    if (cor.name.toLowerCase() === 'date') return new cor();
    if (!isObject(origin)) {
        return origin;
    }
    // 维持原对象的原型链
    const res = new cor();
    for (let k in origin) {
        if (origin.hasOwnProperty(k)) {
            res[k] = deepClone(origin[k], map);
            map.set(origin, res[k]);
        }
    }
    return res;
}

const cloObj = deepClone(p, new WeakMap());
cloObj.introduce();
console.log(p === cloObj,  cloObj);