const gen_random = (mini, maxz, sz) => { // 生成 [mini, maxz] 闭区间的数据
    const res = [];
    while (sz--) {
        res.push(Math.floor(Math.random() * (maxz - mini + 1) + mini));
    }
    return res;
};

const arr = gen_random(0, 10000, 10);
console.log(arr);

// 原地交换快速排序
const quick_sort = (l, r, arr_) => {
    if (l >= r) return;
    let i = l - 1, j = r + 1;
    const x = arr_[l + r >> 1];
    while (i < j) {
        while (arr_[++i] < x) ;
        while (arr_[--j] > x) ;
        if (i < j) {
            let t = arr_[i];
            arr_[i] = arr_[j];
            arr_[j] = t;
        }
    }
    quick_sort(l, j, arr_);
    quick_sort(j + 1, r, arr_);
};

// quick_sort(0, arr.length - 1, arr);
// console.log(arr);

function merge_sort(q, l, r) {
    if (l >= r) return;
    let mid = l + r >> 1;
    merge_sort(q, l, mid);
    merge_sort(q, mid + 1, r);
    let i = l, j = mid + 1;
    let k = 0;
    let temp = new Array(l + r + 1).fill(0);
    while (i <= mid && j <= r) {
        if (q[i] < q[j]) temp[k++] = q[i++];
        else temp[k++] = q[j++];
    }
    while (i <= mid) temp[k++] = q[i++];
    while (j <= r) temp[k++] = q[j++];
    k = 0;
    for (i = l; i <= r; i++) q[i] = temp[k++];
}

// merge_sort(arr, 0, arr.length - 1);
// console.log(arr);

function bubbleSort(q) {
    for (let i = 0; i < q.length; i ++) {
        let flag = true;
        for (let  j = 0; j < q.length - 1; j ++) {
            if (q[j] > q[j + 1]) {
                flag = false;
                [q[j], q[j + 1]] = [q[j + 1], q[j]];
            }
        }
        if (flag) {
            break;
        }
    }
}

bubbleSort(arr);
console.log(JSON.stringify(arr));


