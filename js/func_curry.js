function curry (func) {
    const preArgs = [].slice.call(arguments, 1);
    function curried(...args) {
        const restArgs = [].slice.call(arguments);
        const allArgs = [...preArgs, ...restArgs];
        return curry.call(this, func, ...allArgs);
    }
    curried.toString = function () {
        return func.apply(this, preArgs);
    }
    return curried;
}

function add(...args) {
    return args.reduce((pre, cur) => pre + cur, 0);
}

const f = curry(add);
console.log(f(1)(2)(3, 4, 5)(6).toString());
