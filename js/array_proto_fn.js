Array.prototype.myReduce = function (cb, initVal) {
    if (this === null) {
        throw new TypeError("Array.prototype.reduce " + "called on null or undefined");
    }
    if (typeof cb !== 'function') {
        throw new TypeError(cb + 'is not a function');
    }
    const len = this.length;
    let acc = typeof initVal === 'undefined' ? this[0] : initVal;
    let i = typeof initVal === 'undefined' ? 1 : 0;
    while (i < len) {
        acc = cb(acc, this[i], i, this);
        i++;
    }
    return acc;
}

Array.prototype.myMap = function (fn) {
    if (this === null) {
        throw new TypeError('this must be a arr');
    }

    if (typeof fn !== 'function') {
        throw new TypeError(fn + 'must be a function');
    }

    const o = Object(this);
    const len = o.length >>> 0;
    let k = 0;
    const res = [];
    while (k < len) {
        res.push(fn(o[k], k, o));
        k++;
    }
    return res;
}
// const arr = [
//     {name: 123, age: 20},
//     {name: 'abc', age: 19},
//     {name: 'uuu', age: 20}
// ]
// console.log(arr.myMap((item) => item.name));

Array.prototype.myForEach = function (fn) {
    if (this === null) {
        throw new TypeError('this must be a arr');
    }

    if (typeof fn !== 'function') {
        throw new TypeError(fn + 'must be a function');
    }
    const o = Object(this);
    const len = o.length >>> 0;
    let k = 0;
    while (k < len) {
        fn(o[k], k, o);
        k++;
    }
}

// const arr = [1, 2, 3, 46, 66];
// arr.myForEach(item => console.log(item))

Array.prototype.myFilter = function (fn) {
    if (this === null) {
        throw new TypeError('this must be a arr');
    }

    if (typeof fn !== 'function') {
        throw new TypeError(fn + 'must be a function');
    }
    const o = Object(this);
    const len = o.length >>> 0;
    let k = 0;
    let res = [];
    while (k < len) {
        if (fn(o[k], k, o)) {
            res.push(o[k]);
        }
        k++;
    }
    return res;
}

// const arr = [1, 2, 3, 46, 66];
// console.log(arr.myFilter(item => item % 2 === 0));

Array.prototype.mySome = function (fn) {
    if (this === null) {
        throw new TypeError('this must be a arr');
    }

    if (typeof fn !== 'function') {
        throw new TypeError(fn + 'must be a function');
    }
    const o = Object(this);
    const len = o.length >>> 0;
    let k = 0;
    while (k < len) {
        if (fn(o[k], k, o)) {
            return true;
        }
        k++;
    }
    return false;
}
const arr = [1, 2, 3, 46, 66];
console.log(arr.mySome(item => item > 10000));