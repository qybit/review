class EventBus {
    constructor() {
        this.cache = {};
    }

    on(type, fn) {
        let handler = this.cache[type];
        if (!handler) {
            this.cache[type] = fn;
        } else if (handler && typeof handler !== 'function') {
            this.cache[type] = [handler, fn];
        } else {
            this.cache[type].push(fn);
        }
    }

    emit(name, ...args) {
        const handler = this.cache[name];
        if (Array.isArray(handler)) {
            handler.forEach((fn) => {
                if (fn.length > 0) {
                    fn.apply(this, args);
                } else {
                    fn.call(this);
                }
            })
        } else {
            if (handler.length > 0) {
                handler.apply(this, args);
            } else {
                handler.call(this);
            }
        }
        return true;
    }

    off(type, fn) {
        const handler = this.cache[name];
        if (handler && typeof handler === 'function') {
            this.cache[name] = null;
        } else {
            handler.splice(handler.findIndex(e => e === fn), 1);
        }
    }

    once(type, fn) {
        const o = Object(this);

        function handler() {
            o.off(type, fn);
            fn.apply(null, arguments);
        }

        this.on(type, handler);
    }
}