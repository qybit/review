// 原始 list 如下
let list = [
    {id: 1, name: '部门A', parentId: 0},
    {id: 2, name: '部门B', parentId: 0},
    {id: 3, name: '部门C', parentId: 1},
    {id: 4, name: '部门D', parentId: 1},
    {id: 5, name: '部门E', parentId: 2},
    {id: 6, name: '部门F', parentId: 3},
    {id: 7, name: '部门G', parentId: 2},
    {id: 8, name: '部门H', parentId: 4}
];

function convert(arr) {
    const obj = {};
    const res = [];
    arr.forEach(item => {
        obj[item.id] = item;
    })
    arr.forEach(item => {
        if (item.parentId !== 0) {
            obj[item.parentId]['children'] ? obj[item.parentId]['children'].push(item) : obj[item.parentId]['children'] = [item]
        } else {
            res.push(item);
        }
    })
    return res;
}

// const result = convert(list);
// console.log(JSON.stringify(result));


const handlerKey = (key_) => {
    return key_.replace(/_(\w)/g, (_, letter) => letter.toUpperCase());
}

const isObject = (obj) => (obj && obj instanceof Object);

function converterToComb(obj) {
    if (!isObject(obj)) {
        return obj;
    }
    const res = {};
    const keys = Object.keys(obj);
    for (let key of keys) {
        const value = obj[key];
        const newKey = handlerKey(key);
        res[newKey] = isObject(value) ? converterToComb(value) : value;
    }
    return res;
}

const json_test = {
    "abc_ajsh": {
        "sjhjhf_aksj": {
            "asdjhfh_jhsd_jahsh": "122121",
        },
        "jakc_skhs": 12,
    },
    "asdjfh_kfasdjf": 10,
}

console.log(converterToComb(json_test));