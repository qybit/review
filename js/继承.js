function Person(name, age) {
    this.name = 'abc';
    this.age = age;
}

Person.prototype.getName = function () {
    return this.name;
}

function XiaoMing(gender) {
    Person.call(this); // call super class
    this.gender = gender;
}
// 
XiaoMing.prototype = Object.create(Person.prototype);
XiaoMing.prototype.constructor = XiaoMing;

let xiaoMing = new XiaoMing();

console.log(xiaoMing.getName());