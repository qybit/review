const p1 = () => (new Promise((res) => {
    setTimeout(() => {
        console.log('p1');
        res('p2');
    }, 1000);
}));

function* p() {
    const p2 = yield p1();
    console.log(p2);
    console.log('p3');
}

((generator) => {
    const gen = generator();
    function next(data) {
        const { value, done } = gen.next(data);
        if (done) {
            return value;
        } else if (!(value instanceof Promise)) {
            next(value);
        } else {
            value.then((data) => next(data));
        }
    }
    next();
})(p);