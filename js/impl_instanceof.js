class Animal {

}
class Person extends Animal{

}

class XiaoMing extends Person{

}

/**
 *
 * @param source 某个构造函数实例
 * @param parent 某个构造函数
 * @returns {boolean} bool
 */
function myInstanceof(source, parent) {
    if (typeof source !== 'object' || source === null) return false;
    let sourceProto = Object.getPrototypeOf(source);
    let parentProto = parent.prototype;
    while (true) {
        if (sourceProto === null) return false;
        if (sourceProto === parentProto) return true;
        sourceProto = Object.getPrototypeOf(sourceProto);
    }
}

console.log(myInstanceof(new XiaoMing(), String))