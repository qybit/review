const search = (nums_, x) => {
    let l = 0, r = nums_.length - 1;
    while (l < r) {
        let mid = l + r >> 1;
        if (nums_[mid] >= x) {
            r = mid;
        } else {
            l = mid + 1;
        }
    }
    if (nums_[l] !== x) return -1;
    return l;
};
const nums = [-1, 0, 3, 4, 5, 5, 9, 12];
const target = 9; 
console.log(search(nums, target)); // 6


const obj = {
    txt: undefined,
}

const {txt = ""} = obj;
console.log(txt);
