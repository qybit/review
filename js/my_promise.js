const PENDING = 'PENDING';
const REJECT = 'REJECT';
const RESOLVE = 'RESOLVE';

class MyPromise {
    constructor(executor) {
        this.status = PENDING;
        this.reason = undefined;
        this.value = undefined;
        this.callbacks = [];
        // 初始化立即执行
        executor(this.resolve.bind(this), this.reject.bind(this));
    }

    then(onFulfilled, onRejected) {
        return new MyPromise((nextResolve, nextReject) => {
            this.callbacks.push({
                onFulfilled,
                onRejected,
                nextReject,
                nextResolve
            });
        })
    }

    resolve(value) {
        if (value instanceof MyPromise) {
            value.then(
                this.resolve.bind(this),
                this.reject.bind(this)
            );
            return;
        }
        this.value = value;
        this.status = RESOLVE;
        this.callbacks.forEach((fn) => this.handler(fn));
    }

    reject(reason) {
        if (reason instanceof MyPromise) {
            reason.then(
                this.resolve.bind(this),
                this.reject.bind(this)
            );
            return;
        }
        this.reason = reason;
        this.status = REJECT;
        this.callbacks.forEach((fn) => this.handler(fn));
    }

    handler(fn) {
        const {onFulfilled, onRejected, nextResolve, nextReject} = fn;

        if (this.status === PENDING) {
            this.callbacks.push(fn);
            return;
        }

        if (this.status === RESOLVE) {
            const nextValue = onFulfilled ? onFulfilled(this.value) : undefined;
            nextResolve(nextValue);
            return;
        }

        if (this.status === REJECT) {
            const nextReason = onRejected ? onRejected(this.reason) : undefined;
            nextReject(nextReason);
        }
    }
}

function fetchData(success) {
    return new MyPromise((resolve, reject) => {
        setTimeout(() => {
            if (success) {
                resolve("willem");
            } else {
                reject('error');
            }
        }, 1000);
    });
}

fetchData(true).then(data => {
    console.log(data); // after 1000ms: willem
}).then();

fetchData(false).then(null, (reason) => {
    console.log(reason); // after 1000ms: error
});
