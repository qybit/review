// https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/create

//  测试方法

function Animal(type, name, age) {
    this.type = type;
    this.name = name;
    this.age = age;
}

let a1 = new Animal('猫科', 'niko', 4);
let a2 =  Object.create(a1);
a2.name = 'kiko';
console.log(a2, a1, a1 === a2)
