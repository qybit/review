function testFn() {
    console.log(this);
}

Function.prototype.myBind = function (context = window) {
    if (typeof this !== 'function') {
        throw new TypeError('error');
    }
    const args = [...arguments].slice(1);
    const fn = this;
    return function Fn() {
        // 防止构造 bind 时，传入的是 null
        return fn.apply(this instanceof Fn ? new fn(...arguments) : context, args.concat(...arguments))
    }
}

testFn.call(null);
