function numFormat(n) {
    const num = n.toString().split('.');
    const arr = num[0].split('').reverse();
    const res = [];
    for (let i = 0; i < arr.length; i++) {
        if (i % 3 === 0 && i !== 0) {
            res.push(',');
        }
        res.push(arr[i]);
    }
    res.reverse();
    if (num[1]) {
        return res.join('').concat('.'+num[1]);
    }
    return res.join('');
}

console.log(numFormat(1234567))