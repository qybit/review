function New(func, ...args) {
  const obj = {};

  obj.__proto__ = func.prototype;

  const res = func.apply(obj, args);

  return res instanceof Object ? res : obj;
}


function Person(name, age) {
  this.name = name;
  this.age = age;
}

Person.prototype.say= function() {
  console.log(`my name is ${this.name} and i'm ${this.age} old`);
}

const p1 = New(Person, 'lihua', 16);
p1.say();

const p2 = new Person('lihua2', 14);
p2.say();
