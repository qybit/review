const findK = function (arr, k) {
    const quickSort = function (q, l, r) {
        if (l >= r) return;
        let x = q[l + r >> 1], i = l - 1, j = r + 1;
        while (i < j) {
            while (q[++i] < x) ;
            while (q[--j] > x) ;
            if (i < j) {
                [q[i], q[j]] = [q[j], q[i]];
            }
        }
        quickSort(q, l, j);
        quickSort(q, j + 1, r);
    }
    quickSort(arr, 0, arr.length - 1);
    return arr[k - 1];
};

const arr = [1, 5, 4, 5, 6, 7, 9];
console.log(arr);
console.log(findK(arr, 5));
