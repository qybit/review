const express = require('express');

const app = express();

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false, limit: '20MB' }))

// parse application/json
app.use(express.json())

// 测试jsonp

// app.all('*', function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
//     res.header("X-Powered-By", ' 3.2.1')
//     res.header("Content-Type", "application/json;charset=utf-8");
//     next();
// });

app.get('/test', (req, res) => {
    const { callback } = req.query;
    const data = {
        test: 'ok',
        abc: 123,
    }
    res.send(`${callback}(${JSON.stringify(data)})`)
});

app.post('/test_post', (req, res) => {
    console.log(req);
    console.log(JSON.stringify(req.body))
    res.json({
        msg: 'this is post',
    });
});

app.listen(3000, () => {
    console.log('sever is running...');
});