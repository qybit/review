var colorBorder = function(grid, row, col, color) {
    const m = grid.length, n = grid[0].length;
    const vis = new Array(m).fill(0).map(_ => new Array(n).fill(false));
    const directions = [[1, 0], [-1, 0], [0, 1], [0, -1]];
    const flag = grid[row][col];
    const dfs = (i, j) => {
        if (i < 0 || j < 0 || i >= m || j >= n || vis[i][j] || grid[i][j] !== flag) return;
        grid[i][j] = color;
        vis[i][j] = true;
        console.log(i, '---', j);
        for (let d of directions) {
            dfs(d[0] + i, d[1] + j);
        }
    };
    dfs(row, col);
    return grid;
};

console.log(colorBorder([[1, 1], [1, 2]], 0, 0, 3));