## 所谓布局

可以这样记忆

### 圣杯布局

一个奖杯有俩把手，跟奖杯是一体的。所以html 结构看起来是下面这样的
```html
<div class="container clearfix">
    <div class="main"></div>
    <div class="left"></div>
    <div class="right"></div>
</div>
```

### 双飞翼布局

人本身是不能飞行的，借助翼装，我们可以进行滑翔等极限运动。所以 html 结构看起来是下面这样的

```html
 <div class="clearfix">
    <div class="container">
        <div class="main"></div>
    </div>
    <div class="left"></div>
    <div class="right"></div>
</div>
```
